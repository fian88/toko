<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/cari','BarangController@cari');
Route::get('/cari_id','BarangController@cari_id');

Route::get('/hapus/{id}','BarangController@hapus');
Route::get('/edit/{id}','BarangController@edit');
Route::get('/tambahbarang','BarangController@tambahbarang');
Route::get('/','BarangController@index');
Route::post('/store','BarangController@store');
Route::post('/update','BarangController@update');

Route::get('/tambahstok','BarangController@tambahstok');
Route::get('/penambahanstok/{id}','BarangController@penambahanstok');
Route::post('/updatestok','BarangController@updatestok');

Route::get('kurangistok', 'BarangController@kurangistok');
Route::get('/penguranganstok/{id}','BarangController@penguranganstok');
Route::post('/mengurangistok','BarangController@mengurangistok');


Route::get('/ubah','BarangController@ubah');
Route::get('/hapus','BarangController@lamanHapus');

Route::get('/print', 'BarangController@print');

Route::get('/barang/export_excel', 'BarangController@export_excel');
Route::post('/barang/import_excel', 'BarangController@import_excel');